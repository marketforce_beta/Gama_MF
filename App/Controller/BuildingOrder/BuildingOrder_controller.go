package BuildingOrder

import (
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/taxjar/taxjar-go"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
)

type OrderDetails struct {
	Item  string  `json:"item"`
	Price float64 `json:"price"`
}

type Order struct {
	UserID               int64          `valid:"required,lte=10" json:"user_id"`
	DealerID             int64          `valid:"required,lte=255" json:"dealer_id"`
	InvoiceNumber        int64          `valid:"required,lte=255" json:"invoice_number"`
	InvoiceDate          string         `valid:"required,lte=255" json:"invoice_date"`
	Name                 string         `valid:"required,lte=255" json:"name"`
	Email                string         `valid:"required,lte=255" json:"email"`
	Phone                string         `valid:"required,lte=15" json:"phone"`
	Phone2               string         `valid:"lte=15" json:"phone2"`
	Phone3               string         `valid:"lte=15" json:"phone3"`
	Address              string         `valid:"required,lte=255" json:"address"`
	City                 string         `valid:"required,lte=255" json:"city"`
	StateID              int64          `valid:"required,lte=255" json:"state_id"`
	Zip                  string         `valid:"required,lte=255" json:"zip"`
	Width                float64        `valid:"required,lte=255" json:"width"`
	RoofLength           float64        `valid:"required,lte=255" json:"roof_length"`
	FrameLength          float64        `valid:"required,lte=255" json:"frame_length"`
	LegHeight            float64        `valid:"required,lte=255" json:"leg_height"`
	Gauge                string         `valid:"required,lte=255" json:"gauge"`
	Price                float64        `valid:"required,lte=10" json:"price"`
	Description          string         `valid:"required,lte=255" json:"description"`
	DescriptionPrice     float64        `valid:"required,lte=255" json:"description_price"`
	ColorTop             string         `valid:"lte=255" json:"color_top"`
	ColorSides           string         `valid:"lte=255" json:"color_sides"`
	ColorEnds            string         `valid:"lte=255" json:"color_ends"`
	ColorTrim            string         `valid:"lte=255" json:"color_trim"`
	NonTaxFeesDesc       string         `valid:"lte=255" json:"non_tax_fees_desc"`
	NonTaxFees           float64        `valid:"lte=255" json:"non_tax_fees"`
	InstallationAddress  string         `valid:"required,lte=255" json:"installation_address"`
	InstallationCity     string         `valid:"required,lte=255" json:"installation_city"`
	InstallationStateID  int64          `valid:"required,lte=255" json:"installation_state_id"`
	InstallationZip      string         `valid:"required,lte=255" json:"installation_zip"`
	TotalTax             float64        `valid:"required,lte=255" json:"total_tax"`
	TotalSales           float64        `valid:"lte=255" json:"total_sales"`
	Tax                  float64        `valid:"lte=255" json:"tax"`
	TaxExempt            string         `valid:"lte=255" json:"tax_exempt"`
	Total                float64        `valid:"lte=255" json:"total"`
	Deposit              float64        `valid:"lte=255" json:"deposit"`
	BalanceDue           float64        `valid:"lte=255" json:"balance_due"`
	SpecialInstructions  string         `valid:"lte=255" json:"special_instructions"`
	SurfaceLevel         string         `valid:"lte=255" json:"surface_level"`
	ElectricityAvailable string         `valid:"lte=255" json:"electricity_available"`
	InstallationSurface  string         `valid:"lte=255" json:"installation_surface"`
	Payment              string         `valid:"lte=255" json:"payment"`
	Status               uint           `valid:"lte=256" json:"status"`
	Data                 []OrderDetails `valid:"lte=255" json:"data"`
}

func GetOrders(c *fiber.Ctx) error {
	type Orders struct {
		ID              int64
		InvoiceNumber   int64      `json:"invoice_number"`
		Customer        string     `json:"customer"`
		FirstName       string     `json:"first_name"`
		City            string     `json:"city"`
		State           string     `json:"state"`
		InvoiceDate     *time.Time `json:"invoice_date"`
		CreatedBy       string     `json:"created_by"`
		DateLastUpdated *time.Time `json:"date_last_updated"`
		LastUpdateBy    string     `json:"last_update_by"`
		Status          string     `json:"status"`
		TotalSales      float64    `json:"total_sales"`
		Tax             float64    `json:"tax"`
		ReadyInstall    bool       `json:"ready_install"`
		WeekCreate      int64
		WeekCurrent     int64
	}
	type DetailOrderStatus struct {
		ID           int64
		LastUpdateBy string `json:"last_update_by"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var orders []model.GamaBuildingsOrders
	var listOrders []Orders
	var finalOrders []Orders
	var detailOrderStatus []DetailOrderStatus
	db.Find(&orders)

	for _, v := range orders {
		if erro := db.Raw("SELECT O.id as id, O.invoice_number,O.ready_install, D.name AS first_name, U.name as created_by,  O.name as customer, O.city as city, BS.name as state, O.total_sales as total_sales, O.tax as tax, O.created_at as invoice_date, O.updated_at as date_last_updated, B.name as status, WEEKOFYEAR(O.created_at) AS week_create, WEEKOFYEAR(CURDATE()) AS week_current FROM gama_buildings_orders as O, `gama_buildings_orders_status_details` as A, gama_buildings_statuses as B, gama_dealers as D, users as U, gama_buildings_states as BS WHERE A.order_id = O.id AND O.id = ? AND B.id = A.status_id AND O.dealer_id = D.id AND O.user_id = U.id AND O.state_id = BS.id AND  O.deleted_at IS NULL ORDER BY A.id DESC LIMIT 1", v.ID).Scan(&listOrders).Error; erro != nil {
			return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No orders present", "data": nil})
		}
		db.Raw("SELECT OSD.id as id, U.name as last_update_by FROM gama_buildings_orders_status_details as OSD, users as U where OSD.order_id = ? AND OSD.user_id = U.id ORDER BY id DESC LIMIT 1", v.ID).Scan(&detailOrderStatus)
		finalOrders = append(finalOrders, Orders{ID: listOrders[0].ID, InvoiceNumber: listOrders[0].InvoiceNumber, Customer: listOrders[0].Customer, FirstName: listOrders[0].FirstName, City: listOrders[0].City, State: listOrders[0].State, InvoiceDate: listOrders[0].InvoiceDate, CreatedBy: listOrders[0].CreatedBy, DateLastUpdated: listOrders[0].DateLastUpdated, LastUpdateBy: detailOrderStatus[0].LastUpdateBy, Status: listOrders[0].Status, TotalSales: listOrders[0].TotalSales, Tax: listOrders[0].Tax, WeekCreate: listOrders[0].WeekCreate, WeekCurrent: listOrders[0].WeekCurrent})
	}
	if len(finalOrders) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No orders present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders found", "data": finalOrders})
}

func GetJsonOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order []model.GamaBuildingsOrders
	db.Find(&order)
	if len(order) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No orders present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders found", "data": order})
}

func CreateOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var newOrder Order
	var order model.GamaBuildingsOrders
	if err = c.BodyParser(&newOrder); err != nil {
		log.Println(c.BodyParser(&newOrder))
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": err})
	}
	order.UserID = newOrder.UserID
	order.DealerID = newOrder.DealerID
	order.InvoiceNumber = newOrder.InvoiceNumber
	date, _ := time.Parse("2006-01-02", newOrder.InvoiceDate)
	order.InvoiceDate = date
	order.Name = newOrder.Name
	order.Email = newOrder.Email
	order.Phone = newOrder.Phone
	order.Address = newOrder.Address
	order.City = newOrder.City
	order.StateID = newOrder.StateID
	order.Zip = newOrder.Zip
	order.Width = newOrder.Width
	order.RoofLength = newOrder.RoofLength
	order.FrameLength = newOrder.FrameLength
	order.LegHeight = newOrder.LegHeight
	order.Gauge = newOrder.Gauge
	order.Description = newOrder.Description
	order.DescriptionPrice = newOrder.DescriptionPrice
	order.ColorTop = newOrder.ColorTop
	order.ColorSides = newOrder.ColorSides
	order.ColorEnds = newOrder.ColorEnds
	order.ColorTrim = newOrder.ColorTrim
	order.NonTaxFeesDesc = newOrder.NonTaxFeesDesc
	order.NonTaxFees = newOrder.NonTaxFees
	order.InstallationAddress = newOrder.InstallationAddress
	order.InstallationCity = newOrder.InstallationCity
	order.InstallationStateID = newOrder.InstallationStateID
	order.InstallationZip = newOrder.InstallationZip
	order.TotalTax = newOrder.TotalTax
	order.TotalSales = newOrder.TotalSales
	order.Tax = newOrder.Tax
	order.TaxExempt = newOrder.TaxExempt
	order.Total = newOrder.Total
	order.Deposit = newOrder.Deposit
	order.BalanceDue = newOrder.BalanceDue
	order.SpecialInstructions = newOrder.SpecialInstructions
	order.SurfaceLevel = newOrder.SurfaceLevel
	order.ElectricityAvailable = newOrder.ElectricityAvailable
	order.InstallationSurface = newOrder.InstallationSurface
	order.Payment = newOrder.Payment
	order.Price = newOrder.Price
	if err := db.Create(&order).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating order", "data": err})
	}
	db.First(&order, order.ID)
	if newOrder.Data != nil {
		for _, item := range newOrder.Data {
			var OrderDetail model.GamaBuildingsOrderDetails
			OrderDetail.OrderID = int64(order.ID)
			OrderDetail.Item = item.Item
			OrderDetail.Price = item.Price
			if err := db.Create(&OrderDetail).Error; err != nil {
				return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating Order Detail", "data": err})
			}
		}
	}
	var status model.GamaBuildingsOrdersStatusDetail
	status.OrderID = uint(order.ID)
	status.StatusID = newOrder.Status
	status.UserID = uint(newOrder.UserID)
	status.Date = time.Now()
	if err := db.Create(&status).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating Order Status", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order created", "data": order})
}

func GetOrderById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order model.GamaBuildingsOrders
	var orderDetail []model.GamaBuildingsOrderDetails
	var status model.GamaBuildingsOrdersStatusDetail
	var orderResponse Order
	if err := db.Where("id = ?", c.Params("id")).First(&order).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Order not found", "data": nil})
	}
	orderResponse.UserID = order.UserID
	orderResponse.DealerID = order.DealerID
	orderResponse.InvoiceNumber = order.InvoiceNumber
	orderResponse.InvoiceDate = order.InvoiceDate.Format("2006-01-02")
	orderResponse.Name = order.Name
	orderResponse.Email = order.Email
	orderResponse.Phone = order.Phone
	orderResponse.Phone2 = order.Phone2
	orderResponse.Phone3 = order.Phone3
	orderResponse.Address = order.Address
	orderResponse.City = order.City
	orderResponse.StateID = order.StateID
	orderResponse.Zip = order.Zip
	orderResponse.Width = order.Width
	orderResponse.RoofLength = order.RoofLength
	orderResponse.FrameLength = order.FrameLength
	orderResponse.LegHeight = order.LegHeight
	orderResponse.Gauge = order.Gauge
	orderResponse.Price = order.Price
	orderResponse.Description = order.Description
	orderResponse.DescriptionPrice = order.DescriptionPrice
	orderResponse.ColorTop = order.ColorTop
	orderResponse.ColorSides = order.ColorSides
	orderResponse.ColorEnds = order.ColorEnds
	orderResponse.ColorTrim = order.ColorTrim
	orderResponse.NonTaxFeesDesc = order.NonTaxFeesDesc
	orderResponse.NonTaxFees = order.NonTaxFees
	orderResponse.InstallationAddress = order.InstallationAddress
	orderResponse.InstallationCity = order.InstallationCity
	orderResponse.InstallationStateID = order.InstallationStateID
	orderResponse.InstallationZip = order.InstallationZip
	orderResponse.TotalTax = order.TotalTax
	orderResponse.TotalSales = order.TotalSales
	orderResponse.Tax = order.Tax
	orderResponse.TaxExempt = order.TaxExempt
	orderResponse.Total = order.Total
	orderResponse.Deposit = order.Deposit
	orderResponse.BalanceDue = order.BalanceDue
	orderResponse.SpecialInstructions = order.SpecialInstructions
	orderResponse.SurfaceLevel = order.SurfaceLevel
	orderResponse.ElectricityAvailable = order.ElectricityAvailable
	orderResponse.InstallationSurface = order.InstallationSurface
	orderResponse.Payment = order.Payment
	db.Find(&orderDetail, "order_id = ?", c.Params("id"))
	db.Last(&status, "order_id = ?", c.Params("id"))
	orderResponse.Status = status.StatusID
	var details []OrderDetails
	for _, item := range orderDetail {
		var detail OrderDetails
		detail.Item = item.Item
		detail.Price = item.Price
		details = append(details, detail)
	}
	orderResponse.Data = details
	return c.JSON(fiber.Map{"status": "success", "message": "Order found", "data": orderResponse})
}

func UpdateOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var OrderRequest Order
	var order model.GamaBuildingsOrders
	var Details []model.GamaBuildingsOrderDetails
	if err := c.BodyParser(&OrderRequest); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": err})
	}
	if err := db.Where("id = ?", c.Params("id")).First(&order).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Order not found", "data": nil})
	}
	order.City = OrderRequest.City
	order.StateID = OrderRequest.StateID
	order.Zip = OrderRequest.Zip
	order.Width = OrderRequest.Width
	order.RoofLength = OrderRequest.RoofLength
	order.FrameLength = OrderRequest.FrameLength
	order.LegHeight = OrderRequest.LegHeight
	order.Gauge = OrderRequest.Gauge
	order.Description = OrderRequest.Description
	order.DescriptionPrice = OrderRequest.DescriptionPrice
	order.ColorTop = OrderRequest.ColorTop
	order.ColorSides = OrderRequest.ColorSides
	order.ColorEnds = OrderRequest.ColorEnds
	order.ColorTrim = OrderRequest.ColorTrim
	order.NonTaxFeesDesc = OrderRequest.NonTaxFeesDesc
	order.NonTaxFees = OrderRequest.NonTaxFees
	order.InstallationAddress = OrderRequest.InstallationAddress
	order.InstallationCity = OrderRequest.InstallationCity
	order.InstallationStateID = OrderRequest.InstallationStateID
	order.InstallationZip = OrderRequest.InstallationZip
	order.TotalTax = OrderRequest.TotalTax
	order.TotalSales = OrderRequest.TotalSales
	order.Tax = OrderRequest.Tax
	order.TaxExempt = OrderRequest.TaxExempt
	order.Total = OrderRequest.Total
	order.Deposit = OrderRequest.Deposit
	order.BalanceDue = OrderRequest.BalanceDue
	order.SpecialInstructions = OrderRequest.SpecialInstructions
	order.SurfaceLevel = OrderRequest.SurfaceLevel
	order.ElectricityAvailable = OrderRequest.ElectricityAvailable
	order.InstallationSurface = OrderRequest.InstallationSurface
	order.Payment = OrderRequest.Payment
	db.Save(&order)
	db.Delete(&model.GamaBuildingsOrderDetails{}, "order_id = ?", order.ID)
	if OrderRequest.Data != nil {
		for _, item := range OrderRequest.Data {
			var OrderDetail model.GamaBuildingsOrderDetails
			OrderDetail.OrderID = int64(order.ID)
			OrderDetail.Item = item.Item
			OrderDetail.Price = item.Price
			if err := db.Create(&OrderDetail).Error; err != nil {
				return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating Order Detail", "data": err})
			}
		}
	}
	db.Find(&Details, "order_id = ?", order.ID)
	var status model.GamaBuildingsOrdersStatusDetail
	status.OrderID = uint(order.ID)
	status.StatusID = OrderRequest.Status
	status.Date = time.Now()
	if err := db.Create(&status).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating Order Status", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order updated order", "data": fiber.Map{"order": order, "details": Details, "status": status}})
}

func UpdateOrderStatus(c *fiber.Ctx) error {
	type OrderNewStatus struct {
		OrderID  uint64 `json:"order_id"`
		UserID   uint64 `json:"user_id"`
		StatusID uint64 `json:"status_id"`
		Note     string `json:"note"`
		Date     string `json:"date"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var newStatus OrderNewStatus
	var orderStatus model.GamaBuildingsOrdersStatusDetail

	if err := c.BodyParser(&newStatus); err != nil {
		log.Println(newStatus)
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": nil})
	}
	date, _ := time.Parse("2006-01-02", newStatus.Date)
	orderStatus.OrderID = uint(newStatus.OrderID)
	orderStatus.UserID = uint(newStatus.UserID)
	orderStatus.StatusID = uint(newStatus.StatusID)
	orderStatus.Notes = newStatus.Note
	orderStatus.Date = date
	if err := db.Save(&orderStatus).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error updating order status", "data": orderStatus})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order updated status", "data": orderStatus})
}

func DeleteOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order model.GamaBuildingsOrders
	if err := db.Where("id = ?", c.Params("id")).First(&order).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Order not found", "data": nil})
	}
	if err := db.Delete(&order).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error deleting order", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order deleted", "data": nil})
}

func GetNewInvoiceNumber(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var result int = 0
	row := db.Table("gama_buildings_orders").Select("MAX(invoice_number) as invoice_number").Row()
	row.Scan(&result)
	result++
	return c.JSON(fiber.Map{"status": "success", "message": "Invoice number found", "invoice": result})
}

func GetMapOrders(c *fiber.Ctx) error {
	type MapOrder struct {
		Id        uint
		Order     int64
		Name      string
		Address   string
		City      string
		State     string
		Zip       string
		Dealer    string
		CreatedAt string
		Total     float64
	}

	type StatusResponse struct {
		Name string
	}

	type MapOrderResponse struct {
		Order     int64
		Name      string
		Address   string
		City      string
		State     string
		Zip       string
		Dealer    string
		Total     float64
		StatusRes string
		CreatedAt string
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var result []MapOrder
	var ma []MapOrderResponse
	db.Raw("SELECT A.id, A.invoice_number as 'order', B.name as 'dealer', A.name, A.address, A.city, C.abbrev as state, A.zip, A.balance_due as total, A.created_at FROM gama_buildings_orders as A, gama_dealers as B, gama_buildings_states as C WHERE A.dealer_id = B.id AND A.state_id = C.id AND A.deleted_at IS NULL").Scan(&result)
	if len(result) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Orders not found", "data": nil})
	}
	for _, order := range result {
		var status StatusResponse
		db.Raw("SELECT B.name FROM gama_buildings_orders_status_details as A, gama_buildings_statuses as B WHERE A.status_id = B.id AND A.order_id = ? ORDER BY A.ID DESC LIMIT 1", order.Id).Scan(&status)
		ma = append(ma, MapOrderResponse{
			Order:     order.Order,
			Name:      order.Name,
			Address:   order.Address,
			City:      order.City,
			State:     order.State,
			Zip:       order.Zip,
			Dealer:    order.Dealer,
			Total:     order.Total,
			CreatedAt: order.CreatedAt,
			StatusRes: status.Name,
		})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders found", "data": ma})
}

func GetTaxByZip(c *fiber.Ctx) error {
	client := taxjar.NewClient(taxjar.Config{
		APIKey: "4bbb462570fdf2d4219c80fb0e0378b9",
	})
	zip := c.Params("zip")
	res, err := client.RatesForLocation(zip)
	if err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error getting tax", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Tax found", "data": res.Rate.CombinedRate})
}

func GetOrdersByStatus(c *fiber.Ctx) error {
	type Orders struct {
		ID            int64
		InvoiceNumber int64      `json:"invoice_number"`
		FirstName     string     `json:"first_name"`
		InvoiceDate   *time.Time `json:"invoice_date"`
		Status        string     `json:"status"`
		WeekCreate    int64
		WeekCurrent   int64
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()

	status := c.Params(("status"))
	var orders []model.GamaBuildingsOrders
	var listOrders []Orders
	var finalOrders []Orders
	db.Find(&orders)

	for _, v := range orders {
		db.Raw("SELECT O.id as id, O.invoice_number, D.name AS first_name, O.created_at as invoice_date, B.name as status, WEEKOFYEAR(O.created_at) AS week_create, WEEKOFYEAR(CURDATE()) AS week_current FROM gama_buildings_orders as O, `gama_buildings_orders_status_details` as A, gama_buildings_statuses as B, gama_dealers as D WHERE A.order_id = O.id AND O.id = ? AND B.id = A.status_id AND O.dealer_id = D.id AND O.deleted_at IS NULL ORDER BY A.id DESC LIMIT 1", v.ID).Scan(&listOrders)
		if listOrders[0].Status == status {
			finalOrders = append(finalOrders, Orders{ID: listOrders[0].ID, InvoiceNumber: listOrders[0].InvoiceNumber, FirstName: listOrders[0].FirstName, InvoiceDate: listOrders[0].InvoiceDate, Status: listOrders[0].Status, WeekCreate: listOrders[0].WeekCreate, WeekCurrent: listOrders[0].WeekCurrent})
		}

	}

	if len(finalOrders) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No orders present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders found", "data": finalOrders})
}

func GetInfoCreate(c *fiber.Ctx) error {
	type Orders struct {
		ID              int64
		InvoiceNumber   int64      `json:"invoice_number"`
		Customer        string     `json:"customer"`
		FirstName       string     `json:"first_name"`
		City            string     `json:"city"`
		State           string     `json:"state"`
		InvoiceDate     *time.Time `json:"invoice_date"`
		CreatedBy       string     `json:"created_by"`
		DateLastUpdated *time.Time `json:"date_last_updated"`
		LastUpdateBy    string     `json:"last_update_by"`
		Status          string     `json:"status"`
		TotalSales      float64    `json:"total_sales"`
		Tax             float64    `json:"tax"`
		ReadyInstall    bool       `json:"ready_install"`
		WeekCreate      int64
		WeekCurrent     int64
	}
	type DetailOrderStatus struct {
		ID           int64
		LastUpdateBy string `json:"last_update_by"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var dealers []model.GamaDealer
	db.Find(&dealers)
	if len(dealers) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No dealers present", "data": nil})
	}
	var states []model.GamaDealerStates
	db.Find(&states)
	if len(states) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No states present", "data": nil})
	}
	var result int = 0
	row := db.Table("gama_buildings_orders").Select("MAX(invoice_number) as invoice_number").Row()
	row.Scan(&result)
	result++

	var orders []model.GamaBuildingsOrders
	var listOrders []Orders
	var finalOrders []Orders
	var detailOrderStatus []DetailOrderStatus
	db.Find(&orders)
	for _, v := range orders {
		if erro := db.Raw("SELECT O.id as id, O.invoice_number,O.ready_install, D.name AS first_name, U.name as created_by,  O.name as customer, O.city as city, BS.name as state, O.total_sales as total_sales, O.tax as tax, O.created_at as invoice_date, O.updated_at as date_last_updated, B.name as status, WEEKOFYEAR(O.created_at) AS week_create, WEEKOFYEAR(CURDATE()) AS week_current FROM gama_buildings_orders as O, `gama_buildings_orders_status_details` as A, gama_buildings_statuses as B, gama_dealers as D, users as U, gama_buildings_states as BS WHERE A.order_id = O.id AND O.id = ? AND B.id = A.status_id AND O.dealer_id = D.id AND O.user_id = U.id AND O.state_id = BS.id AND  O.deleted_at IS NULL ORDER BY A.id DESC LIMIT 1", v.ID).Scan(&listOrders).Error; erro != nil {
			return c.JSON(fiber.Map{"status": "success", "message": "Info found", "data": map[string]interface{}{"dealers": dealers, "states": states, "invoice_number": result, "AllOrders": nil}})
		}
		if mistake := db.Raw("SELECT OSD.id as id, U.name as last_update_by FROM gama_buildings_orders_status_details as OSD, users as U where OSD.order_id = ? AND OSD.user_id = U.id ORDER BY id DESC LIMIT 1", v.ID).Scan(&detailOrderStatus).Error; mistake != nil {
			return c.JSON(fiber.Map{"status": "success", "message": "Info found", "data": map[string]interface{}{"dealers": dealers, "states": states, "invoice_number": result, "AllOrders": nil}})
		}
		finalOrders = append(finalOrders, Orders{ID: listOrders[0].ID, InvoiceNumber: listOrders[0].InvoiceNumber, Customer: listOrders[0].Customer, FirstName: listOrders[0].FirstName, City: listOrders[0].City, State: listOrders[0].State, InvoiceDate: listOrders[0].InvoiceDate, CreatedBy: listOrders[0].CreatedBy, DateLastUpdated: listOrders[0].DateLastUpdated, LastUpdateBy: detailOrderStatus[0].LastUpdateBy, Status: listOrders[0].Status, TotalSales: listOrders[0].TotalSales, Tax: listOrders[0].Tax, WeekCreate: listOrders[0].WeekCreate, WeekCurrent: listOrders[0].WeekCurrent})
	}
	if len(finalOrders) == 0 {
		return c.JSON(fiber.Map{"status": "success", "message": "Info found", "data": map[string]interface{}{"dealers": dealers, "states": states, "invoice_number": result, "AllOrders": nil}})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Info found", "data": map[string]interface{}{"dealers": dealers, "states": states, "invoice_number": result, "AllOrders": finalOrders}})
}

func UpdateOrderInstallStatus(c *fiber.Ctx) error {
	type RI struct {
		ReadyInstall bool `json:"ready_install"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var order model.GamaBuildingsOrders
	var ri RI
	findOrder := db.Where("id = ?", c.Params("id")).First(&order)
	if findOrder.Error != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No order found", "data": nil})
	}
	if err = c.BodyParser(&ri); err != nil {
		return c.Status(503).JSON(fiber.Map{"status": "error", "message": "Error parsing the body", "data": err})
	}
	order.ReadyInstall = ri.ReadyInstall
	if err = db.Save(&order).Error; err != nil {
		return c.Status(503).JSON(fiber.Map{"status": "error", "message": "Error updating the order", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Order updated successfully", "data": order.ReadyInstall})
}

func GetOrdersListByStatus(c *fiber.Ctx) error {
	type Orders struct {
		ID              int64
		InvoiceNumber   int64      `json:"invoice_number"`
		Customer        string     `json:"customer"`
		FirstName       string     `json:"first_name"`
		City            string     `json:"city"`
		State           string     `json:"state"`
		InvoiceDate     *time.Time `json:"invoice_date"`
		CreatedBy       string     `json:"created_by"`
		DateLastUpdated *time.Time `json:"date_last_updated"`
		LastUpdateBy    string     `json:"last_update_by"`
		Status          string     `json:"status"`
		TotalSales      float64    `json:"total_sales"`
		Tax             float64    `json:"tax"`
		ReadyInstall    bool       `json:"ready_install"`
		WeekCreate      int64
		WeekCurrent     int64
	}
	type DetailOrderStatus struct {
		ID           int64
		LastUpdateBy string `json:"last_update_by"`
	}
	data := c.Params("database")
	status := c.Params("status")
	utils.ConnectClient(data)
	db := database.DbC
	var orders []model.GamaBuildingsOrders
	var listOrders []Orders
	var finalOrders []Orders
	var detailOrderStatus []DetailOrderStatus
	db.Find(&orders)
	for _, v := range orders {
		if erro := db.Raw("SELECT O.id as id, O.invoice_number,O.ready_install, D.name AS first_name, U.name as created_by,  O.name as customer, O.city as city, BS.name as state, O.total_sales as total_sales, O.tax as tax, O.created_at as invoice_date, O.updated_at as date_last_updated, B.name as status, WEEKOFYEAR(O.created_at) AS week_create, WEEKOFYEAR(CURDATE()) AS week_current FROM gama_buildings_orders as O, `gama_buildings_orders_status_details` as A, gama_buildings_statuses as B, gama_dealers as D, users as U, gama_buildings_states as BS WHERE A.order_id = O.id AND O.id = ? AND B.id = A.status_id AND O.dealer_id = D.id AND O.user_id = U.id AND O.state_id = BS.id AND  O.deleted_at IS NULL  AND B.name = ? ORDER BY A.id DESC LIMIT 1", v.ID, status).Scan(&listOrders).Error; erro != nil {
			return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Error getting orders", "data": nil})
		}
		db.Raw("SELECT OSD.id as id, U.name as last_update_by FROM gama_buildings_orders_status_details as OSD, users as U where OSD.order_id = ? AND OSD.user_id = U.id ORDER BY id DESC LIMIT 1", v.ID).Scan(&detailOrderStatus)
		finalOrders = append(finalOrders, Orders{ID: listOrders[0].ID, InvoiceNumber: listOrders[0].InvoiceNumber, Customer: listOrders[0].Customer, FirstName: listOrders[0].FirstName, City: listOrders[0].City, State: listOrders[0].State, InvoiceDate: listOrders[0].InvoiceDate, CreatedBy: listOrders[0].CreatedBy, DateLastUpdated: listOrders[0].DateLastUpdated, LastUpdateBy: detailOrderStatus[0].LastUpdateBy, Status: listOrders[0].Status, TotalSales: listOrders[0].TotalSales, Tax: listOrders[0].Tax, WeekCreate: listOrders[0].WeekCreate, WeekCurrent: listOrders[0].WeekCurrent})
	}
	if len(finalOrders) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No orders present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Info found", "data": finalOrders})
}
