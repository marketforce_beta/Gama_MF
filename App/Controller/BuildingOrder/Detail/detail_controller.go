package Detail

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
	"log"
	"strconv"
)

func GetDetailById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var detail model.GamaBuildingsOrderDetails
	if err := db.Where("order_id =  ?", c.Params("id")).First(&detail).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error getting detail", "data": err})
	}
	if detail.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No detail found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Detail found", "data": detail})
}

func UpdateDetailById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var detail model.GamaBuildingsOrderDetails
	if err := db.Where("id = ?", c.Params("id")).First(&detail).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error getting detail", "data": err})
	}
	if detail.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No detail found", "data": nil})
	}
	if err := c.BodyParser(&detail); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": err})
	}
	if err := db.Save(&detail).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error updating detail", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Detail updated", "data": detail})
}

func DeleteDetailById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var detail model.GamaBuildingsOrderDetails
	if err := db.Where("id = ?", c.Params("id")).First(&detail).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error getting detail", "data": err})
	}
	if detail.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No detail found", "data": nil})
	}
	if err := db.Delete(&detail).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error deleting detail", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Detail deleted", "data": nil})
}

func GetDetailByOrderId(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var details []model.GamaBuildingsOrderDetails
	if err := db.Where("order_id = ?", c.Params("order")).Find(&details).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "No details present", "data": err})
	}
	if len(details) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No details present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Details found", "data": details})
}

func CreateDetail(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var detail model.GamaBuildingsOrderDetails
	if err := c.BodyParser(&detail); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": err})
	}
	order, rr := strconv.ParseInt(c.Params("order"), 10, 64)
	if rr != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error parsing order", "data": rr})
	}
	detail.OrderID = order
	if err := db.Create(&detail).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error creating detail", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Detail created", "data": detail})
}
