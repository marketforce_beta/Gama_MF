package Notes

import (
	"log"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
)

type NotesList struct {
	ID        uint      `json:"id"`
	Name      string    `json:"name"`
	LastName  string    `json:"last_name"`
	Note      string    `json:"note"`
	CreatedAt time.Time `json:"created_at"`
}

func GetNotes(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	id := c.Params("order")
	var noteList []NotesList
	if err := db.Raw("SELECT A.id, A.note , B.name, B.last_name, A.created_at FROM gama_buildings_orders_notes AS A, users as B WHERE A.user_id = B.id AND A.deleted_at IS NULL AND A.order_id = ?", id).Scan(&noteList).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No notes present for order", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Notes found", "data": noteList})
}

func CreateNote(c *fiber.Ctx) error {
	var err error
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var note model.GamaBuildingsOrdersNotes
	if err = c.BodyParser(&note); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": err})
	}
	order := c.Params("order")
	note.OrderID, err = strconv.ParseInt(order, 0, 64)
	if err := db.Create(&note).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error creating note", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Note created", "data": note})
}

func GetNoteById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var note model.GamaBuildingsOrdersNotes
	if err := db.Where("id = ?", c.Params("id")).First(&note).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error getting note", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Note found", "data": note})
}

func UpdateNotes(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var note model.GamaBuildingsOrdersNotes
	if err := db.Where("id = ?", c.Params("id")).First(&note).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error getting note", "data": err})
	}
	if err := c.BodyParser(&note); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": err})
	}
	if err := db.Save(&note).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error updating note", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Note updated", "data": note})
}

func DeleteNotes(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var note model.GamaBuildingsOrdersNotes
	if err := db.Where("id = ?", c.Params("id")).First(&note).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error getting note", "data": err})
	}
	if err := db.Delete(&note).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error deleting note", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Note deleted", "data": nil})
}
