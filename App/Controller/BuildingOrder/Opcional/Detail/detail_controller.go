package Detail

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
)

func GetOptionalDetailByOptionalId(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optionalDetail []model.GamaBuildingsOrdersOptionalDetails
	db.Find(&optionalDetail, "optional_id = ?", c.Params("id"))
	if len(optionalDetail) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Optional Detail present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optional Detail found", "data": optionalDetail})
}

func CreateOptionalDetail(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optionalDetail model.GamaBuildingsOrdersOptionalDetails
	if err := c.BodyParser(&optionalDetail); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid JSON", "data": nil})
	}
	if err := db.Create(&optionalDetail).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid JSON", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optional Detail created", "data": optionalDetail})
}

func GetOptionalDetailById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optionalDetail model.GamaBuildingsOrdersOptionalDetails
	if err := db.Find(&optionalDetail, "id = ?", c.Params("id")).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Optional Detail present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optional Detail found", "data": optionalDetail})
}

func UpdateOptionalDetail(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optionalDetail model.GamaBuildingsOrdersOptionalDetails
	if err := db.Find(&optionalDetail, "id = ?", c.Params("id")).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Optional Detail present", "data": nil})
	}
	if err := c.BodyParser(&optionalDetail); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid JSON", "data": nil})
	}
	if err := db.Save(&optionalDetail).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid JSON", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optional Detail updated", "data": optionalDetail})
}

func DeleteOptionalDetail(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optionalDetail model.GamaBuildingsOrdersOptionalDetails
	if err := db.Find(&optionalDetail, "id = ?", c.Params("id")).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Optional Detail present", "data": nil})
	}
	if err := db.Delete(&optionalDetail).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid JSON", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optional Detail deleted", "data": nil})
}
