package Opcional

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
)

func GetOptionalOrders(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optionals []model.GamaBuildingsOrdersOptional
	if err := db.Where("order_id = ?", c.Params("id")).First(&optionals).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error getting optionals", "data": err})
	}
	if len(optionals) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No optionals found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optionals found", "data": optionals})
}

func CreateOptionalOrder(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optional model.GamaBuildingsOrdersOptional
	if err := c.BodyParser(&optional); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error creating optional", "data": err})
	}
	if err := db.Create(&optional).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error creating optional", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optional created", "data": optional})
}

func GetOptionalOrderById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optional model.GamaBuildingsOrdersOptional
	if err := db.Where("id = ?", c.Params("id")).First(&optional).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error getting optional", "data": err})
	}
	if optional.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No optional found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optional found", "data": optional})
}

func UpdateOptionalOrderById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optional model.GamaBuildingsOrdersOptional
	if err := db.Where("id = ?", c.Params("id")).First(&optional).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error updating optional", "data": err})
	}
	if optional.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No optional found", "data": nil})
	}
	if err := c.BodyParser(&optional); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error updating optional", "data": err})
	}
	if err := db.Save(&optional).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error updating optional", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optional updated", "data": optional})
}

func DeleteOptionalOrderId(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var optional model.GamaBuildingsOrdersOptional
	if err := db.Where("id = ?", c.Params("id")).First(&optional).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error deleting optional", "data": err})
	}
	if optional.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No optional found", "data": nil})
	}
	if err := db.Delete(&optional).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error deleting optional", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Optional deleted", "data": nil})
}
