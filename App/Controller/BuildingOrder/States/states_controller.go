package States

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
	"log"
)

func GetBuildingStates(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var states []model.GamaBuildingsStates
	db.Find(&states)
	if len(states) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No states present", "data": nil})
	} else {
		return c.JSON(fiber.Map{"status": "success", "message": "Building States found", "data": states})
	}
}
