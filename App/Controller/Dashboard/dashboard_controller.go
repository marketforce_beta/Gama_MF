package Dashboard

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
	"log"
)

func LastTenOrders(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var orders []model.GamaBuildingsOrders
	db.Limit(10).Order("id desc").Find(&orders)
	if len(orders) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No orders present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders found", "data": orders})
}

func TotalOrders(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var orders []model.GamaBuildingsOrders
	db.Select("Sum(total) as total").Find(&orders)
	if len(orders) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No orders present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders found", "data": orders})
}

func DealerNumbers(c *fiber.Ctx) error {
	var count int64
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var dealers []model.GamaDealer
	db.Model(&dealers).Count(&count)
	if count == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No dealers present", "data": 0})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Dealers found", "data": count})
}

func OrdersChart(c *fiber.Ctx) error {
	type Chart struct {
		InvoiceDate string
		Total       float64
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var chart []Chart
	db.Raw("SELECT invoice_date, SUM(total) as total FROM gama_buildings_orders WHERE MONTH(invoice_date) = MONTH(NOW()) AND YEAR(invoice_date) = YEAR(NOW()) AND deleted_at IS NULL GROUP BY invoice_date").Scan(&chart)
	if len(chart) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No orders present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Orders found", "data": chart})
}

func DealersOrders(c *fiber.Ctx) error {
	type DealerOrders struct {
		Name  string `json:"dealer"`
		Order int64  `json:"total"`
	}
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var results []DealerOrders
	db.Raw("SELECT A.name, COUNT(B.id) as 'order' FROM dealers as A, gama_buildings_orders as B WHERE A.id = B.dealer_id GROUP BY A.name ORDER BY COUNT(B.id) DESC LIMIT 7").Scan(&results)
	if len(results) == 0 {
		return c.Status(204).JSON(fiber.Map{"status": "error", "message": "No dealers present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Dealers found", "data": results})
}

func SaleByDay(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var number_installation int64
	var sale_day float64
	var money float64
	db.Raw("SELECT COUNT(*) as number_installation FROM gama_buildings_orders_status_details as A WHERE A.status_id = 3 GROUP BY id").Scan(&number_installation)
	db.Raw("SELECT SUM(total) as sale_day FROM gama_buildings_orders WHERE DAY(invoice_date) = DAY(NOW()) AND deleted_at IS NULL").Scan(&sale_day)
	db.Raw("SELECT SUM(balance_due) as money FROM gama_buildings_orders").Scan(&money)
	return c.JSON(fiber.Map{"status": "success", "message": "Dealers found", "data": fiber.Map{"number_installation": number_installation, "sale_day": sale_day, "money": money}})
}
