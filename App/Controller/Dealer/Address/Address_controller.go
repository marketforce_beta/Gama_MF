package Address

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
	"log"
	"strconv"
)

func GetAddress(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address []model.GamaDealerAddress
	if err := db.Where("dealer_id = ?", c.Params("dealer")).Find(&address).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Address not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Address found", "data": address})
}

func CreateAddress(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address model.GamaDealerAddress

	if err := c.BodyParser(&address); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	dealer := c.Params("dealer")
	address.DealerID, err = strconv.ParseInt(dealer, 0, 64)
	if err = db.Create(&address).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Address created", "data": address})
}

func GetAddressById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address model.GamaDealerAddress
	if err := db.Where("id = ?", c.Params("id")).First(&address).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Address not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Address found", "data": address})
}

func UpdateAddress(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address model.GamaDealerAddress
	if err := c.BodyParser(&address); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	if err = db.Model(&address).Where("id = ?", c.Params("id")).Updates(address).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Address updated", "data": address})
}

func DeleteAddress(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var address model.GamaDealerAddress
	if err := db.Where("id = ?", c.Params("id")).Delete(&address).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Address not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Address deleted", "data": address})
}
