package Housr

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
)

func GetHours(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var hours []model.GamaDealerHours
	if err := db.Where("id = ?", c.Params("id")).First(&hours).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No hours present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Categories found", "data": hours})
}

func GetHourById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var hours model.GamaDealerHours
	if err := db.Where("id = ?", c.Params("id")).First(&hours).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Address not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Hours found", "data": hours})
}

func CreateHours(c *fiber.Ctx) error {
	var err error
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var hours model.GamaDealerHours
	if err := c.BodyParser(&hours); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	dealer := c.Params("dealer")
	hours.DealerID, err = strconv.ParseInt(dealer, 0, 64)
	if err = db.Create(&hours).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error creating hours", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Hours created", "data": hours})
}

func UpdateHours(c *fiber.Ctx) error {
	var err error
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var hours model.GamaDealerHours
	if err := c.BodyParser(&hours); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	dealer := c.Params("dealer")
	hours.DealerID, err = strconv.ParseInt(dealer, 0, 64)
	if err = db.Model(&hours).Where("id = ?", c.Params("id")).Updates(hours).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error updating hours", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Hours updated", "data": hours})
}

func DeleteHours(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var hours model.GamaDealerHours
	if err := db.Where("id = ?", c.Params("id")).First(&hours).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No hours present", "data": nil})
	}
	if err := db.Delete(&hours).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error deleting hours", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Hours deleted", "data": hours})
}
