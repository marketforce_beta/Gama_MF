package Salesman

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
	"strconv"
)

func GetSalesman(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var salesman []model.GamaDealerSalesman
	if err := db.Where("dealer_id = ?", c.Params("dealer")).Find(&salesman).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "No salesman present", "data": nil})
	}
	if len(salesman) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No salesman present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Salesman found", "data": salesman})
}

func CreateSalesman(c *fiber.Ctx) error {
	var err error
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var salesman model.GamaDealerSalesman
	if err := c.BodyParser(&salesman); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": nil})
	}
	dealer := c.Params("dealer")
	salesman.DealerID, err = strconv.ParseInt(dealer, 0, 64)
	if err = db.Create(&salesman).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error creating salesman", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Salesman created", "data": salesman})
}

func GetSalesmanById(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var salesman model.GamaDealerSalesman
	if err := db.Where("id = ?", c.Params("id")).First(&salesman).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No salesman found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Salesman found", "data": salesman})
}

func UpdateSalesman(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var salesman model.GamaDealerSalesman
	if err := db.Where("id = ?", c.Params("id")).First(&salesman).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No salesman found", "data": nil})
	}
	if err := c.BodyParser(&salesman); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error parsing body", "data": nil})
	}
	if err := db.Save(&salesman).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error updating salesman", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Salesman updated", "data": salesman})
}

func DeleteSalesman(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var salesman model.GamaDealerSalesman
	if err := db.Where("id = ?", c.Params("id")).First(&salesman).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No salesman found", "data": nil})
	}
	if err := db.Delete(&salesman).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Error deleting salesman", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Salesman deleted", "data": nil})
}
