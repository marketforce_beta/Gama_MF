package Dealer

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
	"log"
)

func GetDealers(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var dealers []model.GamaDealer
	db.Find(&dealers)
	if len(dealers) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No dealers present", "data": nil})
	} else {
		return c.JSON(fiber.Map{"status": "success", "message": "Dealers found", "data": dealers})
	}
}

func CreateDealer(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var dealer model.GamaDealer
	if err := c.BodyParser(&dealer); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	if err := db.Create(&dealer).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Dealer created", "data": dealer})
}

func GetDealer(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var dealer model.GamaDealer
	if err := db.Where("id = ?", c.Params("id")).First(&dealer).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Dealer not found", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Dealer found", "data": dealer})
}

func UpdateDealer(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var dealer model.GamaDealer
	if err := db.Where("id = ?", c.Params("id")).First(&dealer).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Dealer not found", "data": err})
	}
	if err := c.BodyParser(&dealer); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	if err := db.Save(&dealer).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Dealer updated", "data": dealer})
}

func DeleteDealer(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var dealer model.GamaDealer
	if err := db.Where("id = ?", c.Params("id")).First(&dealer).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Dealer not found", "data": err})
	}
	if err := db.Delete(&dealer).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Dealer deleted", "data": nil})
}
