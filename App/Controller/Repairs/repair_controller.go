package Repair

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/Gama_MF/App/Model"
	utils "gitlab.com/victorrb1015/Gama_MF/App/Utils"
	database "gitlab.com/victorrb1015/Gama_MF/Database"
	"os"
	"time"
)

func GetAllRepairs(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var repairs []model.GamaRepairs
	db.Find(&repairs)
	if len(repairs) == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "No repairs found",
			"data":    nil,
		})

	}
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Repairs found",
		"data":    repairs,
	})
}

func CreateRepair(c *fiber.Ctx) error {
	type repairRequest struct {
		CustomerStatement   string  `valid:"required,lte=255" json:"customer_statement" form:"customer_statement"`
		DealerStatement     string  `valid:"required,lte=255" json:"dealer_statement" form:"dealer_statement"`
		ContractorStatement string  `valid:"required,lte=255" json:"contractor_statement" form:"contractor_statement"`
		RepairDetails       string  `valid:"required,lte=255" json:"repair_details" form:"repair_details"`
		Total               float64 `valid:"required,lte=10.2" json:"total" form:"total"`
		OrderID             int64   `valid:"required,lte=255" json:"order_id" form:"order_id"`
	}

	type imgRepairRequest struct {
		Image    string `valid:"required,lte=255" json:"image" form:"image"`
		RepairID int64  `valid:"required,lte=255" json:"repair_id" form:"repair_id"`
	}

	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var repair model.GamaRepairs
	var repairRequestData repairRequest
	if err := c.BodyParser(&repairRequestData); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"status":  "error",
			"message": "Invalid body",
			"data":    err,
		})
	}
	repair.CustomerStatement = repairRequestData.CustomerStatement
	repair.DealerStatement = repairRequestData.DealerStatement
	repair.ContractorStatement = repairRequestData.ContractorStatement
	repair.RepairDetails = repairRequestData.RepairDetails
	repair.Total = repairRequestData.Total
	repair.OrderID = int64(repairRequestData.OrderID)
	db.Create(&repair)
	file, err := c.FormFile("file")
	if err == nil {
		carpet := "Temp/" + data
		if _, err := os.Stat(carpet); os.IsNotExist(err) {
			os.MkdirAll(carpet, 0755)
			if err != nil {
				return c.JSON(fiber.Map{"status": "error", "message": "Temp not created", "data": err})
			}
		}
		route := "Temp/" + data + "/" + time.Now().Format("20060102150405") + "_" + file.Filename
		err = c.SaveFile(file, route)
		if err != nil {
			return c.JSON(fiber.Map{"status": "error", "message": "File not saved", "data": err})
		}
		msg := utils.UploadFiles(route, data)

		var imgRepair model.GamaRepairImages
		imgRepair.Image = msg
		imgRepair.RepairID = int64(repair.ID)
		db.Create(&imgRepair)

	}

	return c.JSON(fiber.Map{"status": "success", "message": "Repair Created", "data": repair})
}

func GetRepair(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var repair model.GamaRepairs
	id := c.Params("id")
	db.First(&repair, id)
	if repair.ID == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "Repair not found",
			"data":    nil,
		})
	}
	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Repair found",
		"data":    repair,
	})
}

func UpdateRepair(c *fiber.Ctx) error {

	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	defer sqlDB.Close()
	var repair model.GamaRepairs
	if err := c.BodyParser(&repair); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"status":  "error",
			"message": "Invalid body",
			"data":    err,
		})
	}

	id := c.Params("id")
	db.Model(&repair).Where("id = ?", id).Updates(repair)
	return c.JSON(fiber.Map{"status": "success", "message": "Repair Updated", "data": repair})
}

func DeleteRepair(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	sqlDB, err := db.DB()
if err != nil {
	log.Fatalln(err)
}
defer sqlDB.Close()
	var repair model.GamaRepairs
	id := c.Params("id")
	db.First(&repair, id)
	if repair.ID == 0 {
		return c.Status(404).JSON(fiber.Map{
			"status":  "error",
			"message": "Repair not found",
			"data":    nil,
		})
	}
	db.Delete(&repair)
	return c.JSON(fiber.Map{"status": "success", "message": "Repair Deleted", "data": nil})
}
