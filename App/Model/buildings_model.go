package Model

import (
	"time"

	"gorm.io/gorm"
)

type GamaDealerAddress struct {
	gorm.Model
	DealerID int64  `gorm:"type:int;not null" db:"dealer_id" valid:"required,lte=10" json:"dealer_id"`
	Name     string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Address  string `gorm:"type:varchar(100);not null" db:"address" valid:"required,lte=100" json:"address"`
	City     string `gorm:"type:varchar(100);not null" db:"city" valid:"required,lte=100" json:"city"`
	State    string `gorm:"type:varchar(100);not null" db:"state" valid:"required,lte=100" json:"state"`
	Zip      string `gorm:"type:varchar(100);not null" db:"zip" valid:"required,lte=100" json:"zip"`
}

type GamaDealerSalesman struct {
	gorm.Model
	DealerID int64  `gorm:"type:int;not null" db:"dealer_id" valid:"required,lte=10" json:"dealer_id"`
	Name     string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Phone    string `gorm:"type:varchar(20);null" db:"phone" valid:"required,lte=20" json:"phone"`
	Email    string `gorm:"type:varchar(100);null" db:"email" valid:"required,lte=100" json:"email"`
}

type GamaDealerHours struct {
	gorm.Model
	DealerID int64  `gorm:"type:int;not null" db:"dealer_id" valid:"required,lte=10" json:"dealer_id"`
	Day      string `gorm:"type:varchar(100);not null" db:"day" valid:"required,lte=100" json:"day"`
	Open     string `gorm:"type:varchar(100);not null" db:"open" valid:"required,lte=100" json:"open"`
	Close    string `gorm:"type:varchar(100);not null" db:"close" valid:"required,lte=100" json:"close"`
}

type GamaDealerForms struct {
	gorm.Model
	DealerID int64  `gorm:"type:int;not null" db:"dealer_id" valid:"required,lte=10" json:"dealer_id"`
	Name     string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	URL      string `gorm:"type:varchar(255);not null" db:"url" valid:"required,lte=255" json:"url"`
}

type GamaDealerImages struct {
	gorm.Model
	DealerID int64  `gorm:"type:int;not null" db:"dealer_id" valid:"required,lte=10" json:"dealer_id"`
	Name     string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	URL      string `gorm:"type:varchar(255);not null" db:"url" valid:"required,lte=255" json:"url"`
}

type GamaBuildingsOrdersNotes struct {
	gorm.Model
	OrderID int64  `gorm:"type:int;not null" db:"order_id" valid:"required,lte=10" json:"order_id"`
	UserID  int64  `gorm:"type:int;not null" db:"user_id" valid:"required,lte=10" json:"user_id"`
	Note    string `gorm:"type:varchar(255);not null" db:"note" valid:"required,lte=255" json:"note"`
}

type GamaBuildingsOrderDetails struct {
	gorm.Model
	OrderID int64   `gorm:"type:int;not null" db:"order_id" valid:"required,lte=10" json:"order_id"`
	Item    string  `gorm:"type:varchar(255);not null" db:"item" valid:"required,lte=255" json:"item"`
	Price   float64 `gorm:"type:decimal(10,2);not null" db:"price" valid:"required,lte=10" json:"price"`
}

type GamaBuildingsOrdersOptionalDetails struct {
	gorm.Model
	OrderOptionalID int64  `gorm:"type:int;not null" db:"order_optional_id" valid:"required,lte=10" json:"order_optional_id"`
	Name            string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Value           string `gorm:"type:varchar(255);not null" db:"value" valid:"required,lte=255" json:"value"`
}

type GamaBuildingsOrdersOptional struct {
	gorm.Model
	OrderID         int64                                `gorm:"type:int;not null" db:"order_id" valid:"required,lte=10" json:"order_id"`
	Name            string                               `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	OptionalDetails []GamaBuildingsOrdersOptionalDetails `gorm:"foreignkey:OrderOptionalID" json:"optional_details"`
}

type GamaBuildingsOrders struct {
	gorm.Model
	UserID               int64                             `gorm:"type:int;not null" db:"user_id" valid:"required,lte=10" json:"user_id"`
	DealerID             int64                             `gorm:"type:int;not null" db:"dealer_id" valid:"required,lte=255" json:"dealer_id"`
	InvoiceNumber        int64                             `gorm:"type:int;not null" db:"invoice_number" valid:"required,lte=255" json:"invoice_number"`
	InvoiceDate          time.Time                         `gorm:"type:date;null" db:"invoice_date" json:"invoice_date"`
	Name                 string                            `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Email                string                            `gorm:"type:varchar(255);not null" db:"email" valid:"required,lte=255" json:"email"`
	Phone                string                            `gorm:"type:varchar(255);not null" db:"phone" valid:"required,lte=255" json:"phone"`
	Phone2               string                            `gorm:"type:varchar(255);null" db:"phone2" valid:"required,lte=255" json:"phone2"`
	Phone3               string                            `gorm:"type:varchar(255);null" db:"phone3" valid:"required,lte=255" json:"phone3"`
	Address              string                            `gorm:"type:varchar(255);not null" db:"address" valid:"required,lte=255" json:"address"`
	City                 string                            `gorm:"type:varchar(255);not null" db:"city" valid:"required,lte=255" json:"city"`
	StateID              int64                             `gorm:"type:int;not null" db:"state_id" valid:"required,lte=255" json:"state_id"`
	Zip                  string                            `gorm:"type:varchar(255);not null" db:"zip" valid:"required,lte=255" json:"zip"`
	Width                float64                           `gorm:"type:float;not null" db:"width" valid:"required,lte=255" json:"width"`
	RoofLength           float64                           `gorm:"type:float;not null" db:"roof_length" valid:"required,lte=255" json:"roof_length"`
	FrameLength          float64                           `gorm:"type:float;not null" db:"frame_length" valid:"required,lte=255" json:"frame_length"`
	LegHeight            float64                           `gorm:"type:float;not null" db:"leg_height" valid:"required,lte=255" json:"leg_height"`
	Gauge                string                            `gorm:"type:varchar(255);null" db:"gauge" valid:"required,lte=255" json:"gauge"`
	Price                float64                           `gorm:"type:decimal(10,2);not null" db:"price" valid:"required,lte=255" json:"price"`
	Description          string                            `gorm:"type:varchar(255);not null" db:"description" valid:"lte=255" json:"description"`
	DescriptionPrice     float64                           `gorm:"type:float;not null" db:"description_price" valid:"required,lte=255" json:"description_price"`
	ColorTop             string                            `gorm:"type:varchar(255);null" db:"color_top" valid:"lte=255" json:"color_top"`
	ColorSides           string                            `gorm:"type:varchar(255);null" db:"color_sides" valid:"lte=255" json:"color_sides"`
	ColorEnds            string                            `gorm:"type:varchar(255);null" db:"color_ends" valid:"lte=255" json:"color_ends"`
	ColorTrim            string                            `gorm:"type:varchar(255);null" db:"color_trim" valid:"lte=255" json:"color_trim"`
	NonTaxFeesDesc       string                            `gorm:"type:varchar(255);null" db:"non_tax_fees_desc" valid:"lte=255" json:"non_tax_fees_desc"`
	NonTaxFees           float64                           `gorm:"type:float;not null" db:"non_tax_fees" valid:"required,lte=255" json:"non_tax_fees"`
	InstallationAddress  string                            `gorm:"type:varchar(255);not null" db:"installation_address" valid:"lte=255" json:"installation_address"`
	InstallationCity     string                            `gorm:"type:varchar(255);not null" db:"installation_city" valid:"lte=255" json:"installation_city"`
	InstallationStateID  int64                             `gorm:"type:int;not null" db:"installation_state_id" valid:"lte=255" json:"installation_state_id"`
	InstallationZip      string                            `gorm:"type:varchar(255);not null" db:"installation_zip" valid:"lte=255" json:"installation_zip"`
	TotalTax             float64                           `gorm:"type:float;not null" db:"total_tax" valid:"required,lte=255" json:"total_tax"`
	TotalSales           float64                           `gorm:"type:decimal;default:0.00" db:"total_sales" valid:"lte=255" json:"total_sales"`
	Tax                  float64                           `gorm:"type:decimal;default:0.00" db:"tax" valid:"lte=255" json:"tax"`
	TaxExempt            string                            `gorm:"type:varchar(255);null" db:"tax_exempt" valid:"lte=255" json:"tax_exempt"`
	Total                float64                           `gorm:"type:decimal;default:0.00" db:"total" valid:"lte=255" json:"total"`
	Deposit              float64                           `gorm:"type:decimal;default:0.00" db:"deposit" valid:"lte=255" json:"deposit"`
	BalanceDue           float64                           `gorm:"type:decimal;default:0.00" db:"balance_due" valid:"lte=255" json:"balance_due"`
	SpecialInstructions  string                            `gorm:"type:text;null" db:"special_instructions" valid:"lte=255" json:"special_instructions"`
	SurfaceLevel         string                            `gorm:"type:varchar(255);null" db:"surface_level" valid:"lte=255" json:"surface_level"`
	ElectricityAvailable string                            `gorm:"type:varchar(255);null" db:"electricity_available" valid:"lte=255" json:"electricity_available"`
	InstallationSurface  string                            `gorm:"type:varchar(255);null" db:"installation_surface" valid:"lte=255" json:"installation_surface"`
	Payment              string                            `gorm:"type:varchar(255);null" db:"payment" valid:"lte=255" json:"payment"`
	Collection           bool                              `gorm:"type:boolean;default:false" db:"collection" valid:"lte=255" json:"collection"`
	ReadyInstall         bool                              `gorm:"type:boolean;default:false" db:"ready_install" valid:"lte=255" json:"ready_install"`
	Latitude             float64                           `gorm:"type:float;null" db:"latitude" valid:"lte=255" json:"latitude"`
	Longitude            float64                           `gorm:"type:float;null" db:"longitude" valid:"lte=255" json:"longitude"`
	Notes                []GamaBuildingsOrdersNotes        `gorm:"ForeignKey:OrderID"`
	OptionalDetails      []GamaBuildingsOrdersOptional     `gorm:"ForeignKey:OrderID"`
	Details              []GamaBuildingsOrderDetails       `gorm:"ForeignKey:OrderID"`
	Repairs              []GamaRepairs                     `gorm:"ForeignKey:OrderID"`
	Status               []GamaBuildingsOrdersStatusDetail `gorm:"ForeignKey:OrderID"`
}

type GamaDealer struct {
	gorm.Model
	Name                string                `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	PrincipalAddress    string                `gorm:"type:varchar(100);not null" db:"address" valid:"required,lte=255" json:"address"`
	City                string                `gorm:"type:varchar(100);not null" db:"city" valid:"required,lte=255" json:"city"`
	Zip                 string                `gorm:"type:varchar(100);not null" db:"zip" valid:"required,lte=255" json:"zip"`
	Phone               string                `gorm:"type:varchar(20);not null" db:"phone" valid:"required,lte=20" json:"phone"`
	Email               string                `gorm:"type:varchar(100);not null" db:"email" valid:"required,lte=100" json:"email"`
	Website             string                `gorm:"type:varchar(100);null" db:"website" valid:"lte=100" json:"website"`
	Commission          float64               `gorm:"type:decimal(10,2);not null" db:"commission" valid:"required,lte=10.2" json:"commission"`
	OwnerName           string                `gorm:"type:varchar(100);not null" db:"owner_name" valid:"required,lte=100" json:"owner_name"`
	FaxNumber           string                `gorm:"type:varchar(20);null" db:"fax_number" valid:"lte=20" json:"fax_number"`
	CoverageMiles       float64               `gorm:"type:decimal(10,2);not null" db:"coverage_miles" valid:"required,lte=10.2" json:"coverage_miles"`
	Type                string                `gorm:"type:varchar(100);not null" db:"type" valid:"required,lte=100" json:"type"`
	YearsInBusiness     int                   `gorm:"type:int;null" db:"years_in_business" valid:"lte=100" json:"years_in_business"`
	NumberOfLocations   int                   `gorm:"type:int;null" db:"number_of_locations" valid:"lte=100" json:"number_of_locations"`
	EinNumber           string                `gorm:"type:varchar(100);not null" db:"ein_number" valid:"required,lte=100" json:"ein_number"`
	EinFile             string                `gorm:"type:varchar(255);null" db:"ein_file" valid:"lte=255" json:"ein_file"`
	StateDealerID       int64                 `gorm:"type:int;not null" db:"state_id" valid:"required,lte=255" json:"state_id"`
	GamaDealerHours     []GamaDealerHours     `gorm:"ForeignKey:DealerID"`
	GamaDealerForms     []GamaDealerForms     `gorm:"ForeignKey:DealerID"`
	GamaDealerImages    []GamaDealerImages    `gorm:"ForeignKey:DealerID"`
	GamaDealerAddress   []GamaDealerAddress   `gorm:"ForeignKey:DealerID"`
	GamaDealerSalesman  []GamaDealerSalesman  `gorm:"ForeignKey:DealerID"`
	GamaBuildingsOrders []GamaBuildingsOrders `gorm:"ForeignKey:DealerID"`
}

type GamaDealerStates struct {
	gorm.Model
	Name    string       `gorm:"type:varchar(255);null" db:"name" valid:"required,lte=255" json:"name"`
	Abbrev  string       `gorm:"type:varchar(255);null" db:"abbrev" valid:"required,lte=255" json:"abbrev"`
	Dealers []GamaDealer `gorm:"foreignkey:StateDealerID"`
}

type GamaBuildingsStates struct {
	gorm.Model
	Name      string                `gorm:"type:varchar(255);null" db:"name" valid:"required,lte=255" json:"name"`
	Abbrev    string                `gorm:"type:varchar(255);null" db:"abbrev" valid:"required,lte=255" json:"abbrev"`
	Buildings []GamaBuildingsOrders `gorm:"foreignkey:StateID"`
}

type GamaBuildingsOrdersStatusDetail struct {
	gorm.Model
	StatusID uint      `gorm:"type:int;not null" db:"status_id" valid:"required,lte=10" json:"status_id"`
	OrderID  uint      `gorm:"type:int;not null" db:"order_id" valid:"required,lte=10" json:"order_id"`
	UserID   uint      `gorm:"type:int;not null" db:"user_id" valid:"required,lte=10" json:"user_id"`
	Notes    string    `gorm:"type:varchar(255);null" db:"notes" valid:"lte=255" json:"notes"`
	Date     time.Time `gorm:"type:timestamp;null" db:"date" valid:"required" json:"date"`
}

type GamaBuildingsStatus struct {
	gorm.Model
	Name         string                            `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	Position     int                               `gorm:"type:int;not null" db:"position" valid:"required,lte=10" json:"position"`
	OrdersStatus []GamaBuildingsOrdersStatusDetail `gorm:"ForeignKey:StatusID"`
}

type GamaRepairs struct {
	gorm.Model
	CustomerStatement   string             `gorm:"type:varchar(255);null" db:"customer_statement" valid:"required,lte=255" json:"customer_statement"`
	DealerStatement     string             `gorm:"type:varchar(255);null" db:"dealer_statement" valid:"required,lte=255" json:"dealer_statement"`
	ContractorStatement string             `gorm:"type:varchar(255);null" db:"contractor_statement" valid:"required,lte=255" json:"contractor_statement"`
	RepairDetails       string             `gorm:"type:varchar(255);null" db:"repair_details" valid:"required,lte=255" json:"repair_details"`
	Total               float64            `gorm:"type:decimal(10,2);not null" db:"total" valid:"required,lte=10.2" json:"total"`
	OrderID             int64              `gorm:"type:int;not null" db:"order_id" valid:"required,lte=255" json:"order_id"`
	RepairImages        []GamaRepairImages `gorm:"ForeignKey:RepairID"`
}

type GamaRepairImages struct {
	gorm.Model
	Image    string `gorm:"type:varchar(255);null" db:"image" valid:"lte=255" json:"image"`
	RepairID int64  `gorm:"type:int;not null" db:"repair_id" valid:"required,lte=255" json:"repair_id"`
}
