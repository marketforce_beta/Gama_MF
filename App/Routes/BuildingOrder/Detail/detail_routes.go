package Detail

import (
	"github.com/gofiber/fiber/v2"
	DetailController "gitlab.com/victorrb1015/Gama_MF/App/Controller/BuildingOrder/Detail"
)

func SetupDetailRoutes(router fiber.Router) {
	detail := router.Group("/order/detail")
	// Read all details
	detail.Get("/:database/:order", DetailController.GetDetailByOrderId)
	// Create a new detail
	detail.Post("/:database/:order", DetailController.CreateDetail)
	// Get detail by id
	detail.Get("/:database/:id", DetailController.GetDetailById)
	// Get Update detail
	detail.Put("/:database/:id", DetailController.UpdateDetailById)
	// Delete detail by id
	detail.Delete("/:database/:id", DetailController.DeleteDetailById)
}
