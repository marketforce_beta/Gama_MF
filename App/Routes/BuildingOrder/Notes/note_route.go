package Notes

import (
	"github.com/gofiber/fiber/v2"
	NoteController "gitlab.com/victorrb1015/Gama_MF/App/Controller/BuildingOrder/Notes"
)

func SetupOrderNoteRoutes(router fiber.Router) {
	note := router.Group("/order/note")
	//Read all Notes
	note.Get("/:database/:order", NoteController.GetNotes)
	//Create a new Note
	note.Post("/:database/:order", NoteController.CreateNote)
	//Get detail by id
	note.Get("/:database/:id", NoteController.GetNoteById)
	//Get Update detail
	note.Put("/:database/:id", NoteController.UpdateNotes)
	//Delete detail by id
	note.Delete("/:database/:id", NoteController.DeleteNotes)
}
