package Detail

import (
	"github.com/gofiber/fiber/v2"
	DetailController "gitlab.com/victorrb1015/Gama_MF/App/Controller/BuildingOrder/Opcional/Detail"
)

func SetupOrderNoteRoutes(router fiber.Router) {
	detail := router.Group("/order/detail")
	//Read all Notes
	detail.Get("/:database/:order", DetailController.GetOptionalDetailByOptionalId)
	//Create a new Note
	detail.Post("/:database/:order", DetailController.CreateOptionalDetail)
	//Get detail by id
	detail.Get("/:database/:id", DetailController.GetOptionalDetailById)
	//Get Update detail
	detail.Put("/:database/:id", DetailController.UpdateOptionalDetail)
	//Delete detail by id
	detail.Delete("/database/:id", DetailController.DeleteOptionalDetail)
}
