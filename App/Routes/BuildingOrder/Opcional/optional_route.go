package Opcional

import (
	"github.com/gofiber/fiber/v2"
	OptionalController "gitlab.com/victorrb1015/Gama_MF/App/Controller/BuildingOrder/Opcional"
)

func SetupOrderOptionalRoutes(router fiber.Router) {
	optional := router.Group("/order/optional")
	//Read all optionals
	optional.Get("/:database/:order", OptionalController.GetOptionalOrders)
	//Create a new optional
	optional.Post("/:database/:order", OptionalController.CreateOptionalOrder)
	//Get detail by id
	optional.Get("/:database/:id", OptionalController.DeleteOptionalOrderId)
	//Get Update detail
	optional.Put("/:database/:id", OptionalController.UpdateOptionalOrderById)
	//Delete detail by id
	optional.Delete("/:database/:id", OptionalController.DeleteOptionalOrderId)
}
