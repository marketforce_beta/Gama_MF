package States

import (
	"github.com/gofiber/fiber/v2"
	BuildingStateController "gitlab.com/victorrb1015/Gama_MF/App/Controller/BuildingOrder/States"
)

func SetupBuildingStateRoutes(router fiber.Router) {
	state := router.Group("/building/states")

	state.Get("/:database", BuildingStateController.GetBuildingStates)
}
