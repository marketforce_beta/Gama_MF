package BuildingOrder

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/victorrb1015/Gama_MF/App/Controller/BuildingOrder"
)

func SetupBuildingOrdersRoutes(router fiber.Router) {
	order := router.Group("/building_orders")
	// Get Orders
	order.Get("/:database", BuildingOrder.GetOrders)
	// Create Order
	order.Post("/:database", BuildingOrder.CreateOrder)
	// Get Order By ID
	order.Get("/:database/:id", BuildingOrder.GetOrderById)
	// Update Order
	order.Put("/:database/:id", BuildingOrder.UpdateOrder)
	// Delete Order
	order.Delete("/:database/:id", BuildingOrder.DeleteOrder)
	// Update Order Status
	order.Post("/status/:database/:id", BuildingOrder.UpdateOrderStatus)
	//Get Orders By Status
	order.Get("/:database/status/:status", BuildingOrder.GetOrdersByStatus)
	// Update Status Install Order
	upStat := router.Group("/building_orders_status")
	// Update status
	upStat.Put("/:database/:id", BuildingOrder.UpdateOrderInstallStatus)
	// Get Invoice Number
	esp := router.Group("/building_orders/petition")
	esp.Get("/inv/:database", BuildingOrder.GetNewInvoiceNumber)
	// Get Map Orders
	esp.Get("/map/:database", BuildingOrder.GetMapOrders)
	// Get Tax
	tax := router.Group("/tax")
	tax.Get("/:zip", BuildingOrder.GetTaxByZip)

	json := router.Group("building_orders_json")
	json.Get("/:database", BuildingOrder.GetJsonOrder)

	info := router.Group("building_orders_info")
	info.Get("/:database", BuildingOrder.GetInfoCreate)

	// Get Orders By Status
	status := router.Group("/building_orders/status")
	status.Get("/:database/:status", BuildingOrder.GetOrdersListByStatus)
}
