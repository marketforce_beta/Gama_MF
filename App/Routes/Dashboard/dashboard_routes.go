package Dashboard

import (
	"github.com/gofiber/fiber/v2"
	DashboardController "gitlab.com/victorrb1015/Gama_MF/App/Controller/Dashboard"
)

func SetupDashboardRoutes(router fiber.Router) {
	dash := router.Group("/dashboard")
	// last ten orders
	dash.Get("/last_orders/:database", DashboardController.LastTenOrders)
	// TotalOrders
	dash.Get("/total_orders/:database", DashboardController.TotalOrders)
	// DealerNumbers
	dash.Get("/dealer_numbers/:database", DashboardController.DealerNumbers)
	// OrdersChart
	dash.Get("/orders_chart/:database", DashboardController.OrdersChart)
	// DealersOrders
	dash.Get("/dealers_orders/:database", DashboardController.DealersOrders)
	// Get Info
	dash.Get("/info/:database", DashboardController.SaleByDay)
}
