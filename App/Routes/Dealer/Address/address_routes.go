package Address

import (
	"github.com/gofiber/fiber/v2"
	AddressController "gitlab.com/victorrb1015/Gama_MF/App/Controller/Dealer/Address"
)

func SetupDealerAddressRoutes(router fiber.Router) {
	address := router.Group("/dealer/address")
	//Read all Address
	address.Get("/:database/:dealer", AddressController.GetAddress)
	//Create Address
	address.Post("/:database/:dealer", AddressController.CreateAddress)
	// Get Address by ID
	address.Get("/:database/:dealer/:id", AddressController.GetAddressById)
	//Update Address
	address.Put("/:database/:id", AddressController.UpdateAddress)
	//Delete Address
	address.Delete("/:database/:id", AddressController.DeleteAddress)
}
