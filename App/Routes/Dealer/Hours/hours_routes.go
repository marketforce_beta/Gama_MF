package Hours

import (
	"github.com/gofiber/fiber/v2"
	HoursController "gitlab.com/victorrb1015/Gama_MF/App/Controller/Dealer/Hours"
)

func SetupDealerHoursRoutes(router fiber.Router) {
	hours := router.Group("/dealer/hours")
	//Read all Hours
	hours.Get("/:database/:dealer", HoursController.GetHours)
	//Create Hours
	hours.Post("/:database/:dealer", HoursController.CreateHours)
	// Get Hours by ID
	hours.Get("/:database/:dealer/:id", HoursController.GetHourById)
	//Update Hours
	hours.Put("/:database/:id", HoursController.UpdateHours)
	//Delete Hours
	hours.Delete("/:database/:id", HoursController.DeleteHours)
}
