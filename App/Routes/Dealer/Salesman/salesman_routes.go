package Salesman

import (
	"github.com/gofiber/fiber/v2"
	SalesmanController "gitlab.com/victorrb1015/Gama_MF/App/Controller/Dealer/Salesman"
)

func SetupDealerHoursRoutes(router fiber.Router) {
	hours := router.Group("/dealer/hours")
	//Read all Hours
	hours.Get("/:database/:dealer", SalesmanController.GetSalesman)
	//Create Hours
	hours.Post("/:database/:dealer", SalesmanController.CreateSalesman)
	// Get Hours by ID
	hours.Get("/:database/:dealer/:id", SalesmanController.GetSalesmanById)
	//Update Hours
	hours.Put("/:database/:id", SalesmanController.UpdateSalesman)
	//Delete Hours
	hours.Delete("/:database/:id", SalesmanController.DeleteSalesman)
}
