package States

import (
	"github.com/gofiber/fiber/v2"
	DealerStateController "gitlab.com/victorrb1015/Gama_MF/App/Controller/Dealer/States"
)

func SetupDealerStateRoutes(router fiber.Router) {
	state := router.Group("/states")

	state.Get("/:database", DealerStateController.GetStates)
}
