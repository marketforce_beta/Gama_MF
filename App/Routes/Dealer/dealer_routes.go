package Dealer

import (
	"github.com/gofiber/fiber/v2"
	DealersController "gitlab.com/victorrb1015/Gama_MF/App/Controller/Dealer"
)

func SetupDealerRoutes(router fiber.Router) {
	dealer := router.Group("/dealer")
	//Read all dealers
	dealer.Get("/:database", DealersController.GetDealers)
	//Get dealer by id
	dealer.Get("/:database/:id", DealersController.GetDealer)
	//Create dealer
	dealer.Post("/:database", DealersController.CreateDealer)
	//Update dealer
	dealer.Put("/:database/:id", DealersController.UpdateDealer)
	//Delete dealer
	dealer.Delete("/:database/:id", DealersController.DeleteDealer)
}
