package Utils

import (
	"flag"
	"io"
	"log"
	"os"
	"time"

	"cloud.google.com/go/storage"
	"gitlab.com/victorrb1015/Gama_MF/Config"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

func UploadFiles(FilePath string, Folder string) string {
	var (
		projectID = flag.String("project", Config.Config("PROJECT_ID"), "Your Google Cloud Platform project ID")
		bucketName = flag.String("bucket", Config.Config("BUCKET_NAME"), "Name of your Google Cloud Storage bucket")
		fileName   = flag.String("file", FilePath, "File to upload")
	)
	flag.Parse()
	if *projectID == "" {
		log.Fatalf("Please specify a project ID")
	}
	if *bucketName == "" {
		log.Fatalf("Please specify a bucket name")
	}
	if *fileName == "" {
		log.Fatalf("Please specify a file name")
	}
	ctx := context.Background()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile("upheld-beach-343016-c094ea3b5ac1.json"))
	if err != nil {
		return "Could not delete object during cleanup: " + err.Error()
	}
	defer client.Close()
	// Open Local File.
	f, err := os.Open(*fileName)
	if err != nil {
		return "Could not open file: " + err.Error()
	}
	defer f.Close()
	
	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()

	o := client.Bucket(*bucketName).Object(*fileName)
	o = o.If(storage.Conditions{DoesNotExist: true})
	wc := o.NewWriter(ctx)
	if _, err := io.Copy(wc, f); err != nil {
		return "Error Copy: " + err.Error()
	}
	if err := wc.Close(); err != nil {
		return "Writer.Close: " + err.Error()
	}
	return "https://storage.googleapis.com/allamericanmbs/" + FilePath
}