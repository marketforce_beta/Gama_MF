package Utils

import (
	"log"

	data "gitlab.com/victorrb1015/Gama_MF/Database"
)

func ConnectClient(database string) error {
	var err error
	if database == ""{
		log.Println("Database not exist")
	}
	db := Decrypt(database)
	if len(db) > 1{
		err = data.ConnectdbClient(db)
		if err != nil{
			return err
		}
	}
	return err
	
}
