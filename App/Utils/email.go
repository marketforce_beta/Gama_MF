package Utils

import (
	"log"
	"strconv"

	hermes "github.com/matcornic/hermes/v2"
	mail "github.com/xhit/go-simple-mail/v2"
	config "gitlab.com/victorrb1015/Gama_MF/Config"
)

func SendEmail(body hermes.Email, to string, subject string) {
	// Configure hermes by setting a theme and your product info
	h := hermes.Hermes{
		// Optional Theme
		// Theme: new(Default)
		Product: hermes.Product{
			// Appears in header & footer of e-mails
			Name: "MarketForce",
			Link: "allamericanmbs.com",
			// Optional product logo
			Logo: "https://allamericanmbs.com/images/AABMK/Logo_MF.png",
			// Custom copyright notice
			Copyright:   "Copyright © 2022 MarketForce. All rights reserved.",
			TroubleText: "If you’re having trouble, Send Email to info@allamericanmbs.com",
		},
	}
	emailBody, erro := h.GenerateHTML(body)
	if erro != nil {
		panic(erro) // Tip: Handle error with something else than a panic ;)
	}

	port, _ := strconv.Atoi(config.Config("SMTP_PORT"))
	server := mail.NewSMTPClient()
	server.Host = config.Config("SMTP_HOST")
	server.Port = port
	server.Username = config.Config("SMTP_USERNAME")
	server.Password = config.Config("SMTP_PASS")
	server.Encryption = mail.EncryptionTLS

	smtpClient, err := server.Connect()
	if err != nil {
		log.Fatal(err)
	}
	// Create email
	email := mail.NewMSG()
	email.SetFrom("MarketForce By MBS <mf@allamericanmbs.com>")
	email.AddTo(to)
	email.SetSubject(subject)

	email.SetBody(mail.TextHTML, emailBody)

	// Send email
	err = email.Send(smtpClient)
	if err != nil {
		log.Fatal(err)
	}

}

func WelcomeEmail() {
	email := hermes.Email{
		Body: hermes.Body{
			Name: "Jon Snow",
			Intros: []string{
				"Welcome to Marketforce! We're very excited to have you on board.",
			},
			Actions: []hermes.Action{
				{
					Instructions: "To get started with Marketforce, please click here:",
					Button: hermes.Button{
						Color: "#FF0000", // Optional action button color
						Text:  "Enter to Marketforce",
						Link:  "https://marketforceapp.com/",
					},
				},
			},
			Outros: []string{
				"Need help, or have questions? Just reply to this email, we'd love to help.",
			},
		},
	}
	SendEmail(email, "fabian@grupocomunicado.com", "Welcome to Marketforce")
}
