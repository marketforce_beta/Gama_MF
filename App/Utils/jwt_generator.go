package Utils

import (
	"github.com/golang-jwt/jwt/v4"
	config "gitlab.com/victorrb1015/Gama_MF/Config"
	"strconv"
	"time"
)

func GenerateNewAccessToken(username string, email string) (string, error) {
	// Set secret key from ..env file.
	secret := config.Config("JWT_SECRET_KEY")

	// Set expires minutes count for secret key from ..env file.
	minutesCount, _ := strconv.Atoi(config.Config("JWT_SECRET_KEY_EXPIRE_MINUTES_COUNT"))

	// Create a new claims.
	claims := jwt.MapClaims{}

	// Set public claims:
	claims["username"] = username
	claims["email"] = email
	claims["exp"] = time.Now().Add(time.Minute * time.Duration(minutesCount)).Unix()

	// Create a new JWT access token with claims.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate token.
	t, err := token.SignedString([]byte(secret))
	if err != nil {
		// Return error, it JWT token generation failed.
		return "", err
	}

	return t, nil
}
