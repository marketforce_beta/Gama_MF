package Utils

import (
	"github.com/golang-jwt/jwt/v4"
	config "gitlab.com/victorrb1015/Gama_MF/Config"
	"strconv"
	"time"
)

func RefresToken(username string, email string) (string, error) {
	// Set expires minutes count for secret key from .env file.
	minutesCount, _ := strconv.Atoi(config.Config("JWT_SECRET_KEY_EXPIRE_MINUTES_COUNT"))
	refreshToken := jwt.New(jwt.SigningMethodHS256)
	claims := refreshToken.Claims.(jwt.MapClaims)
	claims["username"] = username
	claims["email"] = email
	claims["exp"] = time.Now().Add(time.Minute * time.Duration(minutesCount)).Unix()

	rt, err := refreshToken.SignedString([]byte("secret"))
	if err != nil {
		return "", err
	}
	return rt, nil
}
