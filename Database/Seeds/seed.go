package Seeds

import model "gitlab.com/victorrb1015/Gama_MF/App/Model"

var States = []model.GamaDealerStates{
	{
		Name:   "Alabama",
		Abbrev: "AL",
	},
	{
		Name:   "Alaska",
		Abbrev: "AK",
	},
	{
		Name:   "Arizona",
		Abbrev: "AZ",
	},
	{
		Name:   "Arkansas",
		Abbrev: "AR",
	},
	{
		Name:   "California",
		Abbrev: "CA",
	},
	{
		Name:   "Colorado",
		Abbrev: "CO",
	}, {
		Name:   "Connecticut",
		Abbrev: "CT",
	}, {
		Name:   "Delaware",
		Abbrev: "DE",
	}, {
		Name:   "District of Columbia",
		Abbrev: "DC",
	}, {
		Name:   "Florida",
		Abbrev: "FL",
	}, {
		Name:   "Georgia",
		Abbrev: "GA",
	},
	{
		Name:   "Hawaii",
		Abbrev: "HI",
	},
	{
		Name:   "Idaho",
		Abbrev: "ID",
	},
	{
		Name:   "Illinois",
		Abbrev: "IL",
	},
	{
		Name:   "Indiana",
		Abbrev: "IN",
	},
	{
		Name:   "Iowa",
		Abbrev: "IA",
	}, {
		Name:   "Kansas",
		Abbrev: "KS",
	}, {
		Name:   "Kentucky",
		Abbrev: "KY",
	}, {
		Name:   "Louisiana",
		Abbrev: "LA",
	}, {
		Name:   "Maine",
		Abbrev: "ME",
	}, {
		Name:   "Maryland",
		Abbrev: "MD",
	},
	{
		Name:   "Massachusetts",
		Abbrev: "MA",
	},
	{
		Name:   "Michigan",
		Abbrev: "MI",
	},
	{
		Name:   "Minnesota",
		Abbrev: "MN",
	},
	{
		Name:   "Mississippi",
		Abbrev: "MS",
	},
	{
		Name:   "Missouri",
		Abbrev: "MO",
	},
	{
		Name:   "Montana",
		Abbrev: "MT",
	},
	{
		Name:   "Nebraska",
		Abbrev: "NE",
	},
	{
		Name:   "Nevada",
		Abbrev: "NV",
	},
	{
		Name:   "New Hampshire",
		Abbrev: "NH",
	},
	{
		Name:   "New Jersey",
		Abbrev: "NJ",
	},
	{
		Name:   "New Mexico",
		Abbrev: "NM",
	},
	{
		Name:   "New York",
		Abbrev: "NY",
	},
	{
		Name:   "North Carolina",
		Abbrev: "NC",
	},
	{
		Name:   "North Dakota",
		Abbrev: "ND",
	},
	{
		Name:   "Ohio",
		Abbrev: "OH",
	},
	{
		Name:   "Oklahoma",
		Abbrev: "OK",
	},
	{
		Name:   "Oregon",
		Abbrev: "OR",
	},
	{
		Name:   "Pennsylvania",
		Abbrev: "PA",
	},
	{
		Name:   "Rhode Island",
		Abbrev: "RI",
	},
	{
		Name:   "South Carolina",
		Abbrev: "SC",
	},
	{
		Name:   "South Dakota",
		Abbrev: "SD",
	},
	{
		Name:   "Tennessee",
		Abbrev: "TN",
	},
	{
		Name:   "Texas",
		Abbrev: "TX",
	},
	{
		Name:   "Utah",
		Abbrev: "UT",
	},
	{
		Name:   "Vermont",
		Abbrev: "VT",
	},
	{
		Name:   "Virginia",
		Abbrev: "VA",
	},
	{
		Name:   "Washington",
		Abbrev: "WA",
	},
	{
		Name:   "West Virginia",
		Abbrev: "WV",
	},
	{
		Name:   "Wisconsin",
		Abbrev: "WI",
	},
	{
		Name:   "Wyoming",
		Abbrev: "WY",
	},
}

var StatesB = []model.GamaBuildingsStates{
	{
		Name:   "Alabama",
		Abbrev: "AL",
	},
	{
		Name:   "Alaska",
		Abbrev: "AK",
	},
	{
		Name:   "Arizona",
		Abbrev: "AZ",
	},
	{
		Name:   "Arkansas",
		Abbrev: "AR",
	},
	{
		Name:   "California",
		Abbrev: "CA",
	},
	{
		Name:   "Colorado",
		Abbrev: "CO",
	}, {
		Name:   "Connecticut",
		Abbrev: "CT",
	}, {
		Name:   "Delaware",
		Abbrev: "DE",
	}, {
		Name:   "District of Columbia",
		Abbrev: "DC",
	}, {
		Name:   "Florida",
		Abbrev: "FL",
	}, {
		Name:   "Georgia",
		Abbrev: "GA",
	},
	{
		Name:   "Hawaii",
		Abbrev: "HI",
	},
	{
		Name:   "Idaho",
		Abbrev: "ID",
	},
	{
		Name:   "Illinois",
		Abbrev: "IL",
	},
	{
		Name:   "Indiana",
		Abbrev: "IN",
	},
	{
		Name:   "Iowa",
		Abbrev: "IA",
	}, {
		Name:   "Kansas",
		Abbrev: "KS",
	}, {
		Name:   "Kentucky",
		Abbrev: "KY",
	}, {
		Name:   "Louisiana",
		Abbrev: "LA",
	}, {
		Name:   "Maine",
		Abbrev: "ME",
	}, {
		Name:   "Maryland",
		Abbrev: "MD",
	},
	{
		Name:   "Massachusetts",
		Abbrev: "MA",
	},
	{
		Name:   "Michigan",
		Abbrev: "MI",
	},
	{
		Name:   "Minnesota",
		Abbrev: "MN",
	},
	{
		Name:   "Mississippi",
		Abbrev: "MS",
	},
	{
		Name:   "Missouri",
		Abbrev: "MO",
	},
	{
		Name:   "Montana",
		Abbrev: "MT",
	},
	{
		Name:   "Nebraska",
		Abbrev: "NE",
	},
	{
		Name:   "Nevada",
		Abbrev: "NV",
	},
	{
		Name:   "New Hampshire",
		Abbrev: "NH",
	},
	{
		Name:   "New Jersey",
		Abbrev: "NJ",
	},
	{
		Name:   "New Mexico",
		Abbrev: "NM",
	},
	{
		Name:   "New York",
		Abbrev: "NY",
	},
	{
		Name:   "North Carolina",
		Abbrev: "NC",
	},
	{
		Name:   "North Dakota",
		Abbrev: "ND",
	},
	{
		Name:   "Ohio",
		Abbrev: "OH",
	},
	{
		Name:   "Oklahoma",
		Abbrev: "OK",
	},
	{
		Name:   "Oregon",
		Abbrev: "OR",
	},
	{
		Name:   "Pennsylvania",
		Abbrev: "PA",
	},
	{
		Name:   "Rhode Island",
		Abbrev: "RI",
	},
	{
		Name:   "South Carolina",
		Abbrev: "SC",
	},
	{
		Name:   "South Dakota",
		Abbrev: "SD",
	},
	{
		Name:   "Tennessee",
		Abbrev: "TN",
	},
	{
		Name:   "Texas",
		Abbrev: "TX",
	},
	{
		Name:   "Utah",
		Abbrev: "UT",
	},
	{
		Name:   "Vermont",
		Abbrev: "VT",
	},
	{
		Name:   "Virginia",
		Abbrev: "VA",
	},
	{
		Name:   "Washington",
		Abbrev: "WA",
	},
	{
		Name:   "West Virginia",
		Abbrev: "WV",
	},
	{
		Name:   "Wisconsin",
		Abbrev: "WI",
	},
	{
		Name:   "Wyoming",
		Abbrev: "WY",
	},
}

var Status = []model.GamaBuildingsStatus{
	{
		Name:     "Quote",
		Position: 1,
	},
	{
		Name:     "In Shop",
		Position: 2,
	},
	{
		Name:     "Installing",
		Position: 3,
	},
	{
		Name:     "Installed",
		Position: 4,
	},
	{
		Name:     "Canceled",
		Position: 5,
	},
	{
		Name:     "On Hold",
		Position: 6,
	},
	{
		Name:     "Collections",
		Position: 7,
	},
	
}
