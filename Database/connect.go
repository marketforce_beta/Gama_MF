package Database

import (
	"errors"
	"fmt"
	"gitlab.com/victorrb1015/Gama_MF/App/Model"
	config "gitlab.com/victorrb1015/Gama_MF/Config"
	seed "gitlab.com/victorrb1015/Gama_MF/Database/Seeds"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"strconv"
)

var DbC *gorm.DB

func ConnectdbClient(DbName string) error {
	var err error
	p := config.Config("DB_PORT")
	port, err := strconv.ParseUint(p, 10, 32)

	if err != nil {
		log.Println("Idiot")
	}

	// Connection URL to connect to Client Database
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", config.Config("DB_USER"), config.Config("DB_PASSWORD"), config.Config("DB_HOST"), port, DbName)
	DbC, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Println("failed to connect database client")
		return err
	}
	instance, err := DbC.DB()
	instance.SetMaxIdleConns(10000)
	if DbC.Migrator().HasTable(&Model.GamaDealer{}) && DbC.Migrator().HasTable(&Model.GamaBuildingsOrders{}) {
		fmt.Println("Database " + DbName + " Connected")
	} else {
		if err = DbC.AutoMigrate(
			&Model.GamaDealerHours{},
			&Model.GamaDealerForms{},
			&Model.GamaDealerImages{},
			&Model.GamaDealerAddress{},
			&Model.GamaDealerSalesman{},
			&Model.GamaDealerStates{},
			&Model.GamaBuildingsOrdersNotes{},
			&Model.GamaBuildingsOrdersOptionalDetails{},
			&Model.GamaBuildingsOrdersOptional{},
			&Model.GamaBuildingsOrderDetails{},
			&Model.GamaBuildingsStatus{},
			&Model.GamaBuildingsStates{},
			&Model.GamaBuildingsOrders{},
			&Model.GamaBuildingsOrdersStatusDetail{},
			&Model.GamaDealer{}); err == nil && (DbC.Migrator().HasTable(&Model.GamaBuildingsStates{}) || DbC.Migrator().HasTable(&Model.GamaDealerStates{})) {
			if err := DbC.First(&Model.GamaBuildingsStates{}).Error; errors.Is(err, gorm.ErrRecordNotFound) {
				for i, _ := range seed.StatesB {
					DbC.Create(&seed.StatesB[i])
				}
			}
			if err := DbC.First(&Model.GamaDealerStates{}).Error; errors.Is(err, gorm.ErrRecordNotFound) {
				for i, _ := range seed.States {
					DbC.Create(&seed.States[i])
				}
			}
			if err := DbC.First(&Model.GamaBuildingsStatus{}).Error; errors.Is(err, gorm.ErrRecordNotFound) {
				for i, _ := range seed.Status {
					DbC.Create(&seed.Status[i])
				}
			}
		}
		if err != nil {
			fmt.Println("failed to Migrated database client")
			return err
		}
		fmt.Println(" Database " + DbName + " Migrated ")
		return err
	}
	return err
}
