# Building the binary of the App
FROM golang:1.17-alpine AS build

# `boilerplate` should be replaced with your project name
WORKDIR /go/src/Gama_MF

# Copy all the Code and stuff to compile everything
COPY . .

# Downloads all the dependencies in advance (could be left out, but it's more clear this way)
RUN go mod download

# Builds the application as a staticly linked one, to allow it to run on alpine
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -a -installsuffix cgo -o Gama_MF .

# Moving the binary to the 'final Image' to make it smaller
FROM alpine:latest

WORKDIR /app

# `boilerplate` should be replaced here as well
COPY --from=build /go/src/Gama_MF .

# Exposes port 3000 because our program listens on that port
EXPOSE 3060

CMD [ "./Gama_MF" ]