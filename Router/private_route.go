package Router

import (
	"github.com/gofiber/fiber/v2"
	BuildingOrderRoutes "gitlab.com/victorrb1015/Gama_MF/App/Routes/BuildingOrder"
	OrderDetailRoute "gitlab.com/victorrb1015/Gama_MF/App/Routes/BuildingOrder/Detail"
	NoteRoutes "gitlab.com/victorrb1015/Gama_MF/App/Routes/BuildingOrder/Notes"
	BStates "gitlab.com/victorrb1015/Gama_MF/App/Routes/BuildingOrder/States"
	DashRoutes "gitlab.com/victorrb1015/Gama_MF/App/Routes/Dashboard"
	DealerRoutes "gitlab.com/victorrb1015/Gama_MF/App/Routes/Dealer"
	DealerAddress "gitlab.com/victorrb1015/Gama_MF/App/Routes/Dealer/Address"
	StateRoutes "gitlab.com/victorrb1015/Gama_MF/App/Routes/Dealer/States"
)

func PrivateRoutes(app *fiber.App) {
	api := app.Group("/api")
	BuildingOrderRoutes.SetupBuildingOrdersRoutes(api)
	DealerRoutes.SetupDealerRoutes(api)
	OrderDetailRoute.SetupDetailRoutes(api)
	DealerAddress.SetupDealerAddressRoutes(api)
	NoteRoutes.SetupOrderNoteRoutes(api)
	StateRoutes.SetupDealerStateRoutes(api)
	BStates.SetupBuildingStateRoutes(api)
	DashRoutes.SetupDashboardRoutes(api)
}
